package modules;

public enum Species {
    RoboCat,
    Fish,
    DomesticCat,
    Dog,
    UNKNOWN
}

package modules;

import dao.Family;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.Map;
import java.util.Objects;

public class Human {
    private String name;
    private String surname;
    private long birthDate;
    private int iq;
    private Pet pet;
    private Family family;
    //    private String[][] schedule;
    private Map<String, String> schedule;

    public Human(String name, String surname, long birthDate) {
        this.name = name;
        this.surname = surname;
        this.birthDate = birthDate;
    }


    public Human(String name, String surname, long birthDate, int iq, Pet pet, Map<String, String> schedule) {
        this.name = name;
        this.surname = surname;
        this.birthDate = birthDate;
        this.iq = iq;
        this.pet = pet;
        this.schedule = schedule;


    }

    public Human() {

    }

    public String getName() {
        return name;
    }


    public String getSurname() {
        return surname;
    }


    public long getBirthDate() {
        return birthDate;
    }

    public int getIq() {
        return iq;
    }

    public Pet getPet() {
        return pet;
    }

    public Map<String, String> getSchedule() {
        return schedule;
    }

    public Family getFamily() {
        return family;
    }

    public void greetPet(Pet pet) {
        System.out.println("Hello, " + pet.getNickname());
    }

    public void describePet() {
        System.out.println("I have a " + pet.getSpecies() + " he is " + pet.getAge() + " years old, he is " + ((pet.getTrickLevel() < 50) ? "almost not slowly" : "very slowly"));
    }

    public String describeAge() {
        Date date = new Date(getBirthDate());

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
        String dateText = simpleDateFormat.format(date);
        return dateText;

    }


    @Override
    public String toString() {
        return "Human{" + ",\n" +
                "name='" + name + ",\n" +
                "surname='" + surname + ",\n" +
                "birtDate=" + describeAge() + ",\n" +
                '}';
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj);
    }
}

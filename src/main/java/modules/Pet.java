package modules;

import java.util.Arrays;
import java.util.Set;

public abstract class Pet {

    protected Species species;
    protected String nickname;
    protected int age;
    protected int trickLevel;
    //    protected String[] habits;
    protected Set<String> habits;


    public Pet(Species species, String nickname) {
        this.species = species;
        this.nickname = nickname;
    }

    public Pet() {
    }

    public Pet(String nickname, int age, int trickLevel, Set<String> habits) {
        this.nickname = nickname;
        this.age = age;
        this.trickLevel = trickLevel;
        this.habits = habits;
    }

    public Species getSpecies() {
        return species;
    }

    public String getNickname() {
        return nickname;
    }

    public int getAge() {
        return age;
    }

    public int getTrickLevel() {
        return trickLevel;
    }

    public Set<String> getHabits() {
        return habits;
    }

    abstract public void eat();

    // public void eat() {
    //     System.out.println("I am eating");
    // }

    abstract public void respond();

//    public void foul() {
//        System.out.println("I need to cover it up");
//    }


    @Override
    public String toString() {
        return "Pet{" + ",\n" +
                "nickname=" + nickname + ",\n" +
                "age=" + age + ",\n" +
                "trickLevel=" + trickLevel + ",\n" +
                "habits=" + habits + ",\n" +
                '}' + ",\n";
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj);
    }
}
